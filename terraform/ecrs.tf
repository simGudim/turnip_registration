resource "aws_ecr_repository" "my_ecr_repo" {
  name = "trnip/sqlx_migrations"  # Replace with your preferred repository name

  image_tag_mutability = "MUTABLE"  # Options: MUTABLE or IMMUTABLE
  image_scanning_configuration {
    scan_on_push = true  # Enables image scanning on push
  }
}

resource "aws_ecr_lifecycle_policy" "my_ecr_lifecycle_policy" {
  repository = aws_ecr_repository.my_ecr_repo.name

  policy = <<EOF
{
  "rules": [
    {
      "rulePriority": 1,
      "description": "Expire untagged images after 30 days",
      "selection": {
        "tagStatus": "untagged",
        "countType": "sinceImagePushed",
        "countUnit": "days",
        "countNumber": 30
      },
      "action": {
        "type": "expire"
      }
    }
  ]
}
EOF
}