use std::env;

pub fn base_url_from_env() -> String {
    let args: Vec<String> = env::args().collect();
    let environment =  &args[3];
    println!("environment: {}", &environment);
    match environment.as_str() {
        "dev" => {
            "http://users_api:2001".to_string()
        },
        "local" => {
            "http://localhost:2001".to_string()
        },
        _ => {
            "http://localhost:2001".to_string()
        }
    }

}

#[cfg(test)]
mod tests {
    use super::*;
    use reqwest;
    use reqwest::Url;
    use std::collections::HashMap;

    #[actix_web::test]
    async fn test_insert_user() {
        let mut env_base_url =  base_url_from_env();
        env_base_url.push_str("/user");
        let mut map = HashMap::new();
        map.insert("username", "sgudim101");
        map.insert("password", "new_one3");
        let url = Url::parse(env_base_url.as_str()).unwrap();
        println!("url: {:?}", &url);
        
        let client = reqwest::Client::new();
        let _res = client.post(url)
            .json(&map)
            .send()
            .await
            .unwrap();

        // let result =  client.get(env_base_url)
        //     .send()
        //     .await
        //     .unwrap();

        // println!("result: {:?}", &result);
    }

}