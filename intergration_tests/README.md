## Runing tests on Local

1. Run the tests
```bash
cargo test tests -- --nocapture local
```

## Runing tests on Docker

1. Build the test image
```bash
docker build -f Dockerfile.test -t intergration_tests .
```

2. Run the image within the docker compose network
```bash
docker run --network registration_default intergration_tests
```