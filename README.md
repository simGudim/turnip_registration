# USERS API

## Build the image and push to ecr locally
```bash
aws ecr get-login-password --region ca-central-1 --profile trnip | docker login --username AWS --password-stdin 676180064909.dkr.ecr.ca-central-1.amazonaws.com
```

```bash
docker build -t users_api:latest ./api -f ./api/dockerfiles/Dockerfile.prod
```

```bash
docker tag users_api:latest 676180064909.dkr.ecr.ca-central-1.amazonaws.com/trnip/users_api:latest
```

```bash
docker push 676180064909.dkr.ecr.ca-central-1.amazonaws.com/trnip/users_api:latest
```
