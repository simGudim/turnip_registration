-- Add up migration script here
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS users (
    user_id UUID UNIQUE PRIMARY KEY NOT NULL DEFAULT (uuid_generate_v4()),
    username VARCHAR UNIQUE NOT NULL,
    hashed_password VARCHAR NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT false,
    email VARCHAR UNIQUE NOT NULL,
    last_login TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
)