-- Add up migration script here
CREATE TABLE IF NOT EXISTS chat_messages (
    message_id UUID UNIQUE PRIMARY KEY NOT NULL DEFAULT (uuid_generate_v4()),
    room_id UUID NOT NULL REFERENCES chat_rooms(room_id),
    user_id UUID NOT NULL REFERENCES users(user_id),
    room_message VARCHAR NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT false,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    media_urls TEXT[]
);