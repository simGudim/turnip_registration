-- Add up migration script here
CREATE TABLE IF NOT EXISTS auth_tokens (
    user_id UUID UNIQUE PRIMARY KEY NOT NULL REFERENCES users,
    access_token VARCHAR UNIQUE NOT NULL,
    refresh_token VARCHAR UNIQUE NOT NULL,
    access_expires_at TIMESTAMP WITH TIME ZONE,
    refresh_expires_at TIMESTAMP WITH TIME ZONE,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    refreshed_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
)
