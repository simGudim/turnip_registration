-- Add up migration script here
CREATE TABLE IF NOT EXISTS chat_rooms (
    room_id UUID UNIQUE PRIMARY KEY NOT NULL DEFAULT (uuid_generate_v4()),
    room_name VARCHAR UNIQUE NOT NULL,
    room_description VARCHAR,
    deleted BOOLEAN NOT NULL DEFAULT false,
    media_urls TEXT[],
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);