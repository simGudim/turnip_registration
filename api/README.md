#
# Local Setup for the Service

### Start postgres in docker 
```bash 
docker run -d \
	--name db \
    -e POSTGRES_USER=postgres \
	-e POSTGRES_PASSWORD=simong \
    -e POSTGRES_DB=trnip_clients \
    -e PGPORT=5435 \
	-v postgres_db:/var/lib/postgresql/data \
    -p 5435:5435 \
	postgres:14.1-alpine
```


### New migration
```bash
sqlx migrate add -r <name of the migration>
```

```bash
sqlx migrate run --database-url postgres://postgres:simong@0.0.0.0:5435/trnip_clients
```

```bash
sqlx migrate revert --database-url postgres://postgres:simong@0.0.0.0:5435/trnip_clients
```

## Run a specific migration without sqlx
```bash
psql postgres://postgres:simong@0.0.0.0:5435/trnip_clients -f migrations/20241105201945_add_chat_room.up.sql
```

### Run the application
```bash
cargo watch -x "run -- local" --clear
```

#
# Local Setup for the Service in Docker 

### NOTE
* If new queries were added then this would require to prepare the .sqlx json files for compilation by running
```bash
cargo sqlx prepare
```

### Starting the services,always start in detach mode

```bash
docker compose up --no-deps --build -d
```

** If changes to the db configuration take place it is best to rebuild the image
```bash
docker compose build --no-cache
```

### Running migrations

1. Build the migration imgae
```bash
docker build -f dockerfiles/Dockerfile.migration -t migration .
```

2. Get the netowkr name of docker compose
```bash
docker network ls
```

3. Run the migration image on the docker compose network
```bash
docker run -it --network usersAPINetwork migration
```

4. Debugging the postgres instance
```bash
docker exec -it db_container bash
```

5. Removing the volumes to clear the db
```bash
docker volume rm postgres_db
```