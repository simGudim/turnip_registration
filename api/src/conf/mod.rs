use config::ConfigError;
use dotenv::dotenv;
use serde::Deserialize;
use std::env;


#[derive(Debug, Deserialize)]
pub struct Config {
    pub host: String,
    pub port: String,
    pub database_url: String
}

impl Config {
    pub fn from_env() -> Result<Config, ConfigError> {
        // get which environment the application is ran in
        let args: Vec<String> = env::args().collect();
        if args.contains(&"local".to_owned()) {
            dotenv().ok();
        }

        // load the configf into struct
        let mut config = config::Config::new();
        config.merge(config::Environment::default())?;
        config.try_into()
    }
}