use crate::AppState;
use crate::utils::crypto::CryptoService;
use crate::db::{models::{AuthServiceResponseModel, UserModel}, check_user_by_username, insert_user};
use actix_web::{HttpResponse, Result, post, web, http};
use reqwest;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env;
use tracing::{info, error};
use uuid::Uuid;

const INDEX: &'static str = "insert_users";
const SERVICE: &'static str = "users_api";

#[derive(Serialize, Deserialize, Debug)]
pub struct IncomingRequest {
    pub username: String,
    pub password: String,
    pub email: String
}

impl IncomingRequest {
    async fn validate(&self) -> UserModel {
        UserModel {
            user_id : Uuid::new_v4(),
            username : self.username.clone(),
            hashed_password: CryptoService::hash_password(
                self.password.clone()
            ).await.expect("something went wrong during hashing"),
            deleted: false,
            email: CryptoService::encrypt_data(self.email.as_str()),
            last_login: None,
            created_at: None,
            updated_at: None
        }
    }
}

type Request = web::Json<IncomingRequest>;

/*
Exampe Usage:
```bash
curl --header "Content-Type: application/json" \
--data '{"username" : "sgudim", "password" : "new_one", "email" : "simongudim111@yahoo.ca"}' \
http://localhost:2001/user
```
*/

#[post("/user")]
pub async fn insert_user_endpoint(pool: web::Data<AppState>, request: Request) -> Result<HttpResponse> {
    let user = request.validate().await;
    let db_pool = pool.db.clone();
    let user_exists = check_user_by_username(&db_pool, &user.username).await;

    if user_exists.as_ref().is_err() {
        if user_exists.as_ref().unwrap_err() == "User doesn't exist" {
            let insert_result = insert_user(&db_pool, &user).await;
            if insert_result.is_ok() {
                let inserted_user_id = insert_result.as_ref().unwrap().user_id;
                let mut auth_endpoint_base_url = env::var("AUTH_API_ENDPOINT").expect("auth url is not set");
                auth_endpoint_base_url.push_str("/create_tokens");
                let mut auth_payload = HashMap::new();
                auth_payload.insert("user_id", inserted_user_id);

                let client = reqwest::Client::new();
                let tokens_result = client.post(auth_endpoint_base_url)
                    .json(&auth_payload)
                    .send()
                    .await;

                match tokens_result {
                    Ok(tokens_response) => {
                        let tokens = tokens_response.json::<AuthServiceResponseModel>().await;
                        match tokens {
                            Ok(t) => {
                                info!(
                                    target: "insert_user_endpoint",
                                    status_code = 200, 
                                    status = "SUCCESS", 
                                    service = SERVICE,
                                    index = INDEX,
                                    username = user.username
                                );
                                Ok(HttpResponse::Ok()
                                    .status(http::StatusCode::OK)
                                    .json(t)
                                )
                            },
                            Err(t_e) => {
                                error!(
                                    target: "insert_user_endpoint",
                                    message = format!("Something went wrong with tokens repsponse: {}", t_e),
                                    status_code = 500, 
                                    status = "ERROR", 
                                    service = SERVICE,
                                    index = INDEX,
                                    username = user.username
                                );
                                Ok(HttpResponse::ExpectationFailed()
                                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                                    .body(format!("Something went wrong with tokens repsponse:{}", t_e))
                                )
                            }
                        }
                    },
                    Err(tokens_error) => {
                        error!(
                            target: "insert_user_endpoint",
                            message = format!("Something went wrong with creating access tokens: {}", tokens_error),
                            status_code = 500, 
                            status = "ERROR", 
                            service = SERVICE,
                            index = INDEX,
                            username = user.username
                        );
                        Ok(HttpResponse::ExpectationFailed()
                            .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                            .body(format!("Something went wrong with creating access tokens: {}", tokens_error))
                        )
                    }
                }
            } else {
                error!(
                    target: "insert_user_endpoint",
                    message = format!("Something went wrong with the insert: {}", insert_result.as_ref().unwrap_err()),
                    status_code = 500, 
                    status = "ERROR", 
                    service = SERVICE,
                    index = INDEX,
                    username = user.username
                );
                Ok(HttpResponse::ExpectationFailed()
                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(format!("Something went wrong with the insert: {}", insert_result.unwrap_err()))
                )
            }
        } else {
            error!(
                target: "insert_user_endpoint",
                message = format!("User query returned an error: {}", user_exists.as_ref().unwrap_err()),
                status_code = 500, 
                status = "ERROR", 
                service = SERVICE,
                index = INDEX,
                username = user.username
            );
            Ok(HttpResponse::ExpectationFailed()
                .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                .body(format!("User query returned an error: {}", user_exists.unwrap_err()))
            )
        }
    } else {
        info!(
            target: "insert_user_endpoint",
            message = format!("User with username {} alerady exists!", &user.username),
            status_code = 409, 
            status = "CONFLICT", 
            service = SERVICE,
            index = INDEX,
            username = user.username
        );
        Ok(HttpResponse::Conflict()
            .status(http::StatusCode::CONFLICT)
            .body(format!("User with username {} alerady exists!", &user.username))
        )
    }
}

