use crate::AppState;
use crate::utils::crypto::CryptoService;
use crate::db::{get_user_by_username, update_user_last_login_field};
use crate::db::models::AuthServiceResponseModel;

use actix_web::{HttpResponse, Result, post, web, http};
use chrono::Utc;
use serde::Deserialize;
use std::env;
use std::collections::HashMap;
use tracing::{info, error};

const INDEX: &'static str = "validate_user";
const SERVICE: &'static str = "users_api";

#[derive(Deserialize, Debug)]
pub struct IncomingRequest {
    username: String,
    password: String,
}
type Request = web::Json<IncomingRequest>;


/*
Exampe Usage:
```bash
curl --header "Content-Type: application/json" \
--data '{"username" : "sgudim", "password" : "new_one"}' \
http://localhost:2001/user/validate
```
*/

#[post("/user/validate")]
pub async fn validate_user_endpoint(pool: web::Data<AppState>, request: Request) -> Result<HttpResponse> {
    let db_pool = pool.db.clone();
    let user_result = get_user_by_username(&db_pool, &request.username,).await;
    match user_result {
        Ok(user) => {
            let user_verified = CryptoService::verify_password(
                user.hashed_password.as_str(), 
                request.password.as_str()
            ).await;
            if user_verified {
                // Get new auth tokens
                let mut auth_endpoint_base_url = env::var("AUTH_API_ENDPOINT").expect("auth url is not set");
                auth_endpoint_base_url.push_str("/update_tokens");
                let mut auth_payload = HashMap::new();
                auth_payload.insert("user_id", &user.user_id);

                let client = reqwest::Client::new();
                let tokens_result = client.post(auth_endpoint_base_url)
                    .json(&auth_payload)
                    .send()
                    .await;

                match tokens_result {
                    Ok(tokens_response) => {
                        let tokens = tokens_response.json::<AuthServiceResponseModel>().await;
                        match tokens {
                            Ok(t) => {
                                let new_login_timestamp = Utc::now();
                                let _ = update_user_last_login_field(&db_pool, &user.user_id, &new_login_timestamp).await;
                                info!(
                                    target: "validate_user_endpoint",
                                    status_code = 200, 
                                    status = "SUCCESS", 
                                    service = SERVICE,
                                    index = INDEX,
                                    username = request.username,
                                    verified = user_verified
                                );
                                Ok(HttpResponse::Ok()
                                    .status(http::StatusCode::OK)
                                    .json(t)
                                )
                            },
                            Err(t_e) => {
                                error!(
                                    target: "validate_user_endpoint",
                                    message = format!("Something went wrong with tokens repsponse: {}", t_e),
                                    status_code = 500, 
                                    status = "ERROR", 
                                    service = SERVICE,
                                    index = INDEX,
                                    username = user.username
                                );
                                Ok(HttpResponse::ExpectationFailed()
                                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                                    .body(format!("Something went wrong with tokens repsponse:{}", t_e))
                                )
                            }
                        }
                    },
                    Err(tokens_error) => {
                        error!(
                            target: "validate_user_endpoint",
                            message = format!("Something went wrong with updating tokens: {}", tokens_error),
                            status_code = 500, 
                            status = "ERROR", 
                            service = SERVICE,
                            index = INDEX,
                            username = user.username
                        );
                        Ok(HttpResponse::ExpectationFailed()
                            .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                            .body(format!("Something went wrong with creating access tokens: {}", tokens_error))
                        )
                    }
                }
            } else {
                info!(
                    target: "validate_user_endpoint",
                    message = format!("Password is incorrect"),
                    status_code = 403, 
                    status = "ERROR", 
                    service = SERVICE,
                    index = INDEX,
                    username = user.username
                );
                Ok(HttpResponse::ExpectationFailed()
                    .status(http::StatusCode::FORBIDDEN)
                    .body(format!("Password is incorrect"))
                )
            }
        },
        Err(err) => {
            if err == "User doesn't exist" {
                info!(
                    target: "validate_user_endpoint",
                    message = "User doesn't exist",
                    status_code = 404, 
                    status = "NOT_FOUND", 
                    service = SERVICE,
                    index = INDEX,
                    username = request.username
                );
                return Ok(HttpResponse::NotFound()
                    .status(http::StatusCode::NOT_FOUND)
                    .body(format!("user with username {} does not exist", &request.username)))
            } else {
                error!(
                    target: "validate_user_endpoint",
                    message = format!("User query returned an error: {}", err),
                    status_code = 500, 
                    status = "ERROR", 
                    service = SERVICE,
                    index = INDEX,
                    username = request.username
                );
                Ok(HttpResponse::ExpectationFailed()
                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(format!("User query returned an error: {}", err))
                )
            }
        }
    }
}

