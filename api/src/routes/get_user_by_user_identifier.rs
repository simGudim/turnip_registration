use crate::AppState;
use crate::db::models::{UserModel, UserResponseModel};
use crate::utils::crypto::CryptoService;
use crate::db::{get_user_by_username, get_user_by_user_id};
use actix_web::{HttpResponse, Result, get, web, http};
use serde::Deserialize;
use uuid::Uuid;
use tracing::{info, error};

const INDEX: &'static str = "get_users";
const SERVICE: &'static str = "users_api";

#[derive(Deserialize)]
struct QueryParams {
    query_type: String
}



/*
Example Usage:
curl http://localhost:2001/users_api/user/sgudim?query_type=username
*/
#[get("/user/{user_identifier}")]
pub async fn get_user_by_user_identifier_endpoint(
    pool: web::Data<AppState>, 
    path: web::Path<String>, 
    query_params: web::Query<QueryParams>
) -> Result<HttpResponse> {
    let user_identifier = path.into_inner();
    let db_pool = pool.db.clone();
    let user_result: Result<UserModel, String>;

    if query_params.query_type.as_str() == "username" {
        user_result = get_user_by_username(
            &db_pool, 
            &user_identifier
        ).await;
    } else if query_params.query_type.as_str() == "user_id" {
        user_result = get_user_by_user_id(
            &db_pool, 
            &Uuid::parse_str(user_identifier.as_str()).unwrap()
        ).await;
    } else {
        info!(
            target: "get_user_by_username_endpoint",
            message = format!("Qeury type {} doesn't exist", query_params.query_type),
            status_code = 404, 
            status = "NOT_FOUND", 
            service = SERVICE,
            index = INDEX,
            user_identifier = user_identifier
        );
        return Ok(HttpResponse::NotFound()
            .status(http::StatusCode::NOT_FOUND)
            .body(format!("Qeury type {} doesn't exist", query_params.query_type))
        )
    }

    match user_result {
        Ok(mut user) => {
            let decrypted_email = CryptoService::decrypt_data(&user.email);
            if decrypted_email.is_ok() {
                user.email = decrypted_email.unwrap();
                info!(
                    target: "get_user_by_username",
                    status_code = 200, 
                    status = "SUCCESS", 
                    service = SERVICE,
                    index = INDEX,
                    username = user.username,
                    deleted = user.deleted
                );
                let reponse = UserResponseModel::from(user);
                return Ok(HttpResponse::Ok()
                    .status(http::StatusCode::OK)
                    .json(reponse)
                )
            } else {
                error!(
                    target: "get_user_by_username",
                    status_code = 500, 
                    status = "ERROR", 
                    service = SERVICE,
                    index = INDEX,
                    username = user.username,
                    deleted = user.deleted
                );
                return Ok(HttpResponse::InternalServerError()
                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(format!("Could not decrypt the email: {}", decrypted_email.unwrap_err()))
                )
            }
        },
        Err(err) => {
            if err == "User doesn't exist" {
                info!(
                    target: "get_user_by_username_endpoint",
                    message = "User doesn't exist",
                    status_code = 404, 
                    status = "NOT_FOUND", 
                    service = SERVICE,
                    index = INDEX,
                    user_identifier = user_identifier
                );
                return Ok(HttpResponse::NotFound()
                    .status(http::StatusCode::NOT_FOUND)
                    .body(format!("user with user_identifier {} does not exist", &user_identifier)))
            } else {
                error!(
                    target: "get_user_by_username_endpoint",
                    message = format!("User query returned an error: {}", err),
                    status_code = 500, 
                    status = "ERROR", 
                    service = SERVICE,
                    index = INDEX,
                    user_identifier = user_identifier
                );
                Ok(HttpResponse::ExpectationFailed()
                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(format!("User query returned an error: {}", err))
                )
            }
        }
    }
}