use crate::AppState;
use crate::db::{get_user_by_user_id, update_user_deleted_field};
use actix_web::{HttpResponse, Result, post, web, http};
use serde::{Deserialize, Serialize};
use tracing::{info, error};
use uuid::Uuid;

const INDEX: &'static str = "delete_users";
const SERVICE: &'static str = "users_api";


#[derive(Serialize, Deserialize, Debug)]
pub struct IncomingRequest {
    pub user_id: Uuid,
    pub deleted: bool,
}

type Request = web::Json<IncomingRequest>;

/*
Example Usage:
curl --header "Content-Type: application/json" \
--data '{"user_id" : "96715025-bda9-47df-9441-e01846c9368f", "deleted" : false}' \
http://localhost:2001/users_api/user/update-deleted
*/
#[post("/user/update-deleted")]
pub async fn update_user_deleted_field_endpoint(pool: web::Data<AppState>, request: Request) -> Result<HttpResponse> {
    let db_pool = pool.db.clone();
    let user_result = get_user_by_user_id(&db_pool, &request.user_id).await;
    match user_result {
        Ok(user) => {
            let update_result = update_user_deleted_field(&db_pool, &request.user_id, &request.deleted).await;
            if update_result.is_ok() {
                let user = update_result.unwrap();
                info!(
                    target: "update_user_deleted_field_endpoint",
                    status_code = 200, 
                    status = "SUCCESS", 
                    service = SERVICE,
                    index = INDEX,
                    username = user.username,
                    deleted = user.deleted
                );
                return Ok(HttpResponse::Ok()
                    .status(http::StatusCode::OK)
                    .json(user)
                )
            } else {
                error!(
                    target: "update_user_deleted_field_endpoint",
                    status_code = 500, 
                    status = "ERROR", 
                    service = SERVICE,
                    index = INDEX,
                    username = user.username,
                    deleted = user.deleted
                );
                return Ok(HttpResponse::InternalServerError()
                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(format!("Could not update the deleted field: {}", update_result.unwrap_err()))
                )
            }
        },
        Err(err) => {
            if err == "User doesn't exist" {
                info!(
                    target: "update_user_deleted_field_endpoint",
                    message = "User doesn't exist",
                    status_code = 404, 
                    status = "NOT_FOUND", 
                    service = SERVICE,
                    index = INDEX,
                    user_id = request.user_id.to_string(),
                    deleted = request.deleted
                );
                return Ok(HttpResponse::NotFound()
                    .status(http::StatusCode::NOT_FOUND)
                    .body(format!("User with user_id {} does not exist", &request.user_id)))
            } else {
                error!(
                    target: "update_user_deleted_field_endpoint",
                    message = format!("User update returned an error: {}", err),
                    status_code = 500, 
                    status = "ERROR", 
                    service = SERVICE,
                    index = INDEX,
                    user_id = request.user_id.to_string(),
                    deleted = request.deleted
                );
                Ok(HttpResponse::ExpectationFailed()
                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(format!("User query returned an error: {}", err))
                )
            }
        }
    }
}