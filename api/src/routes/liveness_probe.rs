use actix_web::{HttpResponse, Result, get, http};
use tracing::info;

/*
Example Usage:
curl http://localhost:2001/users_api/liveness_probe
*/
#[get("/liveness_probe")]
pub async fn ping_liveness() -> Result<HttpResponse> {
    info!(
        target: "ping_liveness",
        status_code = 200, 
        status = "SUCCESS", 
    );
    Ok(HttpResponse::Ok()
        .status(http::StatusCode::OK)
        .body("It is alive!"))
}