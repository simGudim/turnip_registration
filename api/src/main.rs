mod conf;
mod db;
mod routes;
mod utils;

extern crate magic_crypt;
extern crate futures;
extern crate argonautica;
use crate::conf::Config;

use actix_cors::Cors;
use actix_web::{web, App,HttpServer, http};  
use sqlx::{Pool, Postgres};
use tracing_subscriber;

fn routes(cfg: &mut web::ServiceConfig) {
    let scope = web::scope("/users_api")
        .service(routes::liveness_probe::ping_liveness)
        .service(routes::get_user_by_user_identifier::get_user_by_user_identifier_endpoint)
        .service(routes::insert_user::insert_user_endpoint)
        .service(routes::validated_user::validate_user_endpoint)
        .service(routes::delete_user::update_user_deleted_field_endpoint);
    cfg.service(scope);
}

pub struct AppState {
    db: Pool<Postgres>
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let config = Config::from_env().expect("Server configuration not set");
    tracing_subscriber::fmt().json().init();
    let pool = db::establish_connection().await;   

    HttpServer::new(move || {
        let cors = Cors::default()
            .allowed_origin("http://localhost:8000")
            .allowed_origin("http://localhost:3000")
            .allowed_methods(vec!["GET", "POST"])
            .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
            .allowed_header(http::header::CONTENT_TYPE)
            .max_age(3600);
        App::new()
            .wrap(cors)
            .app_data(web::Data::new(AppState{
                db: pool.clone()
            }))
            .configure(routes)
    })
    .bind(format!("{}:{}", config.host, config.port))?
    .run()
    .await?;
    Ok(())
}
