pub mod models;

use models::UserModel;
use sqlx::{self, Pool, Postgres, postgres::PgPoolOptions};
use tracing::error;
use uuid::Uuid;
use chrono::{DateTime, Utc};

pub async fn establish_connection() -> Pool<Postgres> {
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    match PgPoolOptions::new()
        .max_connections(10)
        .connect(&database_url)
        .await
    {
        Ok(pool) => {
            return pool
        }
        Err(err) => {
            error!("🔥 Failed to connect to the {} database: {:?}", database_url, err);
            std::process::exit(1);
        }
        
    };
}

pub async fn insert_user(db: &Pool<Postgres>, user: &UserModel) -> Result<UserModel, sqlx::Error> {
    let query_result = sqlx::query_as!(
        UserModel,
        r#"
            INSERT INTO users (user_id, username, hashed_password, email) 
            VALUES ($1, $2, $3, $4) RETURNING *
        "#,
        user.user_id,
        user.username,
        user.hashed_password,
        user.email
    )
    .fetch_one(db)
    .await;

    match query_result {
        Ok(record) => Ok(record),
        Err(err) => {
            error!("Query returned with error: {:?}", err);
            Err(err)
        }
    }
}

pub async fn check_user_by_username(db: &Pool<Postgres>, username: &String) -> Result<String, String> {
    let query_result = sqlx::query!(
        r#"
            SELECT username FROM users 
            WHERE username = $1
        "#,
        username
    )
    .fetch_one(db)
    .await;
     
    match query_result {
        Ok(record) => Ok(record.username),
        Err(sqlx::Error::RowNotFound) => {
            Err(format!("User doesn't exist"))
        },
        Err(err) => {
            error!("Query returned with error: {:?}", err);
            Err(format!("Somehting went wrong: {:?}", err))
        }
    }
}

pub async fn get_user_by_username(db: &Pool<Postgres>, username: &String) -> Result<UserModel, String> {
    let query_result = sqlx::query_as!(
        UserModel,
        r#"
            SELECT * FROM users 
            WHERE username = $1
        "#,
        username
    ).fetch_one(db)
    .await;
     
    match query_result {
        Ok(record) => Ok(record),
        Err(sqlx::Error::RowNotFound) => {
            Err(format!("User doesn't exist"))
        },
        Err(err) => {
            error!("Query returned with error: {:?}", err);
            Err(format!("Somehting went wrong: {:?}", err))
        }
    }
}

pub async fn get_user_by_user_id(db: &Pool<Postgres>, user_id: &Uuid) -> Result<UserModel, String> {
    let query_result = sqlx::query_as!(
        UserModel,
        r#"
            SELECT * FROM users 
            WHERE user_id = $1
        "#,
        user_id
    ).fetch_one(db)
    .await;
     
    match query_result {
        Ok(record) => Ok(record),
        Err(sqlx::Error::RowNotFound) => {
            Err(format!("User doesn't exist"))
        },
        Err(err) => {
            error!("Query returned with error: {:?}", err);
            Err(format!("Somehting went wrong: {:?}", err))
        }
    }
}


pub async fn update_user_deleted_field(db: &Pool<Postgres>, user_id: &Uuid, deleted: &bool) -> Result<UserModel, String> {
    let query_result = sqlx::query_as!(
        UserModel,
        r#"
            UPDATE users SET
                deleted = $1,
                updated_at = $2
            WHERE user_id = $3
            RETURNING *
        "#,
        deleted,
        Utc::now(),
        user_id
    ).fetch_one(db)
    .await;
     
    match query_result {
        Ok(record) => Ok(record),
        Err(sqlx::Error::RowNotFound) => {
            Err(format!("User doesn't exist"))
        },
        Err(err) => {
            error!("Update returned with error: {:?}", err);
            Err(format!("Somehting went wrong: {:?}", err))
        }
    }
}

pub async fn update_user_last_login_field(db: &Pool<Postgres>, user_id: &Uuid, last_login: &DateTime<Utc>) -> Result<UserModel, String> {
    let query_result = sqlx::query_as!(
        UserModel,
        r#"
            UPDATE users SET
                last_login = $1
            WHERE user_id = $2
            RETURNING *
        "#,
        last_login,
        user_id
    ).fetch_one(db)
    .await;
     
    match query_result {
        Ok(record) => Ok(record),
        Err(sqlx::Error::RowNotFound) => {
            Err(format!("User doesn't exist"))
        },
        Err(err) => {
            error!("Update returned with error: {:?}", err);
            Err(format!("Somehting went wrong: {:?}", err))
        }
    }
}