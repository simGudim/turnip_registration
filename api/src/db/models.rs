use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use uuid::Uuid;

#[derive(Debug, FromRow, Deserialize, Serialize)]
#[allow(non_snake_case)]
pub struct UserModel {
    pub user_id: Uuid,
    pub username: String,
    pub hashed_password: String,
    pub deleted: bool,
    pub email: String,
    pub last_login: Option<chrono::DateTime<chrono::Utc>>,
    pub created_at: Option<chrono::DateTime<chrono::Utc>>,
    pub updated_at: Option<chrono::DateTime<chrono::Utc>>,
}

#[derive(Debug, Serialize)]
pub struct UserResponseModel {
    pub user_id: Uuid,
    pub username: String,
    pub deleted: bool,
    pub email: String,
    pub last_login: Option<chrono::DateTime<chrono::Utc>>,
    pub created_at: Option<chrono::DateTime<chrono::Utc>>,
    pub updated_at: Option<chrono::DateTime<chrono::Utc>>,
}

impl From<UserModel> for UserResponseModel {
    fn from(a: UserModel) -> Self {
        Self {
            user_id: a.user_id,
            username: a.username,
            deleted: a.deleted,
            email: a.email,
            last_login: a.last_login,
            created_at: a.created_at,
            updated_at: a.updated_at
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct AuthServiceResponseModel {
    pub user_id: Uuid,
    pub access_token: String,
    pub refresh_token: String
}






