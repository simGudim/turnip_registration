
use argonautica::{Hasher, Verifier};
use futures::Future;
use magic_crypt::{new_magic_crypt, MagicCryptTrait, MagicCryptError};
use std::env;


pub struct CryptoService();

impl CryptoService {
    pub async fn hash_password(password: String) -> Result<String, String> {
        let key = env::var("LOGIN_SECRET_KEY").expect("could not find the login secret key");
        let future = Hasher::default()
            .with_secret_key(key)
            .with_password(password)
            .hash_non_blocking()
            .and_then(|produced_hash| Ok(produced_hash))
            .map_err(|err| format!("Hahsing error: {}", err));
    
        future.wait()
    }
    
    pub async fn verify_password(password_hash: &str, password: &str) -> bool {
        let key = env::var("LOGIN_SECRET_KEY").expect("could not find the login secret key");
        let mut verifier = Verifier::default();
        let future = verifier.with_hash(password_hash)
            .with_password(password)
            .with_secret_key(key)
            .verify_non_blocking()
            .and_then(|is_valid| Ok(is_valid))
            .map_err(|err| format!("Verifying error: {}", err));

        future.wait().unwrap_or(false)
    }

    pub fn encrypt_data(data: &str) -> String {
        let key = env::var("DATA_ENCRYPTION_KEY").expect("could not find the login secret key");
        let mc = new_magic_crypt!(key, 256);
        return mc.encrypt_str_to_base64(data)
    }

    pub fn decrypt_data(data: &str) -> Result<String, MagicCryptError> {
        let key = env::var("DATA_ENCRYPTION_KEY").expect("could not find the login secret key");
        let mc = new_magic_crypt!(key, 256);
        return mc.decrypt_base64_to_string(data)
    }

}