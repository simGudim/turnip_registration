# insert user 
```bash
curl --header "Content-Type: application/json" \
--data '{"username" : "sgudim110", "password" : "simong", "email" : "simongudim111@yahoo.ca"}' \
http://localhost:2001/users_api/user
# http://192.168.49.2:31054/users_api/user
```

# get user by user identifier (username)
```bash
curl http://localhost:2001/users_api/user/sgudim110?query_type=username
# curl http://192.168.49.2:31054/users_api/user/sgudim110?query_type=username
```

# get user by user identifier (user_id)
```bash
curl http://localhost:2001/users_api/user/1190a39d-ed83-4355-a30c-837f8b1d4d58?query_type=user_id
```

# validate user
```bash
curl --header "Content-Type: application/json" \
--data '{"username" : "sgudim110", "password" : "simong"}' \
http://192.168.49.2:31054/users_api/user/validate
# http://localhost:2001/users_api/user/validate
```

# update user deleted field
```bash
curl --header "Content-Type: application/json" \
--data '{"user_id" : "96715025-bda9-47df-9441-e01846c9368f", "deleted" : false}' \
http://localhost:2001/users_api/user/update-deleted
```