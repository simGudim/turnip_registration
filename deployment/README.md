# Install virtualBox

```bash
sudo apt remove --purge virtualbox
sudo apt install virtualbox
sudo apt install virtualbox-dkms
```

# Minikube commands
### Start minkube
```bash
minikube start --vm-driver=virtualbox --disk-size 50000mb
```

### Adding a node to the cluster
```bash
minikube node add
```

### Expose the service
```bash
minikube service <service name>
```

# Kubectl command
### Create a namespace
```bash
kubectl create namespace trnip-clients
```

### Delete the secret
```bash
kubectl delete secret ecr-secret -n trnip

```

### Create a login secret to pull the images from ECR
```bash
kubectl --namespace trnip create secret docker-registry ecr-secret \
--docker-server="676180064909.dkr.ecr.ca-central-1.amazonaws.com" \
--docker-username="AWS" \
--docker-password="$(aws ecr get-login-password --region ca-central-1 --profile trnip)"
```

### Deploy postgres
```bash
kubectl apply -f deployment/postgres/postgres-pv.yaml
kubectl apply -n trnip -f deployment/postgres/postgres-pvc.yaml
kubectl apply -n trnip -f deployment/postgres/postgres-deployment.yaml
kubectl apply -n trnip -f deployment/postgres/postgres-service.yaml
```

### Delete all postgres
```bash
kubectl delete -f deployment/postgres/postgres-pv.yaml
kubectl delete -n trnip -f deployment/postgres/postgres-pvc.yaml
```

```bash
kubectl delete deployment postgres-deployment -n trnip
```

### Deploy the users api
```bash
kubectl apply -n trnip -f deployment/deployment.yaml
kubectl apply -n trnip -f deployment/service.yaml
```

### Deploy the migrations for dev
```bash
kubectl apply -n trnip -f deployment/migrations/migrations-deployment.yaml
```

### Delete the deployment
```bash
kubectl delete deployment users-api-deployment -n trnip
```

### Get the pods
```bash
kubectl get pods -n trnip
```

### Get the logs from pod
```bash
kubectl logs <pod-name> -n trnip
```

### Get into the pod for the api
```bash
kubectl exec -n trnip <pods-name> -it --  /bin/bash
```

### Delete the pod
```bash
 kubectl delete pod <pod-name> -n trnip-clients
```

### Describe the pod
```bash
 kubectl describe pod <pod-name> -n trnip

```

# Docker command
### Clean all images
```bash
docker rmi -f $(docker image ls -a -q)

```
